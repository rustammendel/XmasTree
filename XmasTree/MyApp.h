#pragma once

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "Libs/gCamera.h"
#include "Libs/gShaderProgram.h"
#include "Libs/gVertexBuffer.h"
#include "Libs/Mesh_OGL3.h"

class CMyApp {
public:
    CMyApp(void);

    ~CMyApp(void);


    bool Init();

    void Clean();

    void Update();

    void Render();

    void KeyboardDown(SDL_KeyboardEvent &);

    void KeyboardUp(SDL_KeyboardEvent &);

    void MouseMove(SDL_MouseMotionEvent &);

    void MouseDown(SDL_MouseButtonEvent &);

    void MouseUp(SDL_MouseButtonEvent &);

    void MouseWheel(SDL_MouseWheelEvent &);

    void Resize(int, int);

protected:

    GLuint GenTexture();

    GLuint m_textureID;
    GLuint m_textureID_tree;

    void add_triangle(const glm::vec3 &P1,
                      const glm::vec3 &P2,
                      const glm::vec3 &P3,
                      gVertexBuffer &buffer);

    void SurfaceEquation(float u, float v, glm::vec3 &point, glm::vec3 &normal, glm::vec2 &texture);

    void genParametricSurface(int n, gVertexBuffer &buffer);

    void create2Dplane(gVertexBuffer &_bObj, float size);


    void DrawCube(glm::mat4 transf);

    void DrawPrism(glm::mat4 transf, int isBird);

    void DrawTree(glm::mat4 transf);

    void DrawSnow(glm::mat4 transf, glm::mat4 scale);

    void Draw2DPlane(glm::mat4 matW);

    void DrawSphere(glm::mat4 matW, int clrindex);


    gShaderProgram m_program;
    gCamera m_camera;

    gVertexBuffer m_vb; // plane
    //gVertexBuffer	m_vb_octa; // octahedron
    gVertexBuffer m_vb_sphere; // sphere
    gVertexBuffer m_vb_prism;
    gVertexBuffer m_vb_cube;

    Mesh *m_mesh;
    Mesh *m_mesh_tree;

    bool stomping = false;
    int t0;
    int selected = -1;

    int snowcnt = 200;
    std::vector<float> snowXarr, snowYarr, snowZarr;

    int planesize = 30;

    float time = SDL_GetTicks() / 1000.0;
    float inittime;

    float randMax = static_cast <float> (RAND_MAX);
    const float sphereResolution = 20;
    std::vector<glm::vec4> sphereColors;

    glm::vec3 scareCrowPosition = glm::vec3(0, 0, 0);
};
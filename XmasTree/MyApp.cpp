#include "MyApp.h"
#include "Libs/GLUtils.hpp"

#include <GL/glu.h>
#include <math.h>

#include "Libs/ObjParser_OGL3.h"

CMyApp::CMyApp(void) {
    m_textureID = 0;
    m_textureID_tree = 0;
    m_mesh = 0;
    m_mesh_tree = 0;
    srand(time);
    for (size_t i = 0; i < snowcnt; i++) {
        snowXarr.push_back(-planesize / 2 + rand() % planesize);
        snowYarr.push_back((100 + rand() % 200) / 100.0);
        snowZarr.push_back(-planesize / 2 + rand() % planesize);
    }
    for (size_t i = 0; i < 16; i++) {
        sphereColors.push_back(glm::vec4(rand() / randMax, rand() / randMax, rand() / randMax, 1));

    }
}


CMyApp::~CMyApp(void) {
}


GLuint CMyApp::GenTexture() {
    unsigned char tex[256][256][3];

    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < 256; ++j) {
            tex[i][j][0] = rand() % 256;
            tex[i][j][1] = rand() % 256;
            tex[i][j][2] = rand() % 256;
        }

    GLuint tmpID;

    glGenTextures(1, &tmpID);
    glBindTexture(GL_TEXTURE_2D, tmpID);
    gluBuild2DMipmaps(GL_TEXTURE_2D,
                      GL_RGB8, 256,
                      256,            // define storage on the GPU: RGB, 8bits per channel, 256x256 texture
                      GL_RGB, GL_UNSIGNED_BYTE,    // define storate in RAM: RGB layout, unsigned bytes for each channel
                      tex);                        //						  and source pointer
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);    // bilinear filter on min and mag
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    return tmpID;
}

void CMyApp::SurfaceEquation(float u, float v, glm::vec3 &point, glm::vec3 &normal, glm::vec2 &texcoord) {
    float r = 2;
    u *= 2 * 3.1415f;
    v *= 3.1415f;
    float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);

    point = glm::vec3(r * cu * sv, r * cv, r * su * sv);
    normal = glm::vec3(cu * sv, cv, su * sv);
    texcoord = glm::vec2(u, v);
}

void CMyApp::genParametricSurface(int n, gVertexBuffer &buffer) {
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j) {
            float u = i / (float) n, u_ = (i + 1) / (float) n, v = j / (float) n, v_ = (j + 1) / (float) n;
            glm::vec3 P1, P2, P3, P4, N1, N2, N3, N4;
            glm::vec2 T1, T2, T3, T4;

            SurfaceEquation(u, v, P1, N1, T1);
            SurfaceEquation(u_, v, P2, N2, T2);
            SurfaceEquation(u, v_, P3, N3, T3);
            SurfaceEquation(u_, v_, P4, N4, T4);

            buffer.AddData(0, P1);
            buffer.AddData(0, P2);
            buffer.AddData(0, P3);

            buffer.AddData(0, P3);
            buffer.AddData(0, P2);
            buffer.AddData(0, P4);

            buffer.AddData(1, N1);
            buffer.AddData(1, N2);
            buffer.AddData(1, N3);

            buffer.AddData(1, N3);
            buffer.AddData(1, N2);
            buffer.AddData(1, N4);

            buffer.AddData(2, T1);
            buffer.AddData(2, T2);
            buffer.AddData(2, T3);

            buffer.AddData(2, T3);
            buffer.AddData(2, T2);
            buffer.AddData(2, T4);

            buffer.AddIndex((i * n + j) * 6 + 0, (i * n + j) * 6 + 1, (i * n + j) * 6 + 2);
            buffer.AddIndex((i * n + j) * 6 + 3, (i * n + j) * 6 + 4, (i * n + j) * 6 + 5);
        }
}


void CMyApp::add_triangle(
        const glm::vec3 &P1,
        const glm::vec3 &P2,
        const glm::vec3 &P3,
        gVertexBuffer &buffer) {

    buffer.AddData(0, P1.x, P1.y, P1.z); // P1
    buffer.AddData(0, P2.x, P2.y, P2.z); // P2
    buffer.AddData(0, P3.x, P3.y, P3.z); // P3

    glm::vec3 V1 = P2 - P1; //P2-P1
    glm::vec3 V2 = P3 - P1; //P3-P1
    glm::vec3 normal = glm::normalize(glm::cross(V1, V2));

    //normals
    buffer.AddData(1, normal.x, normal.y, normal.z);
    buffer.AddData(1, normal.x, normal.y, normal.z);
    buffer.AddData(1, normal.x, normal.y, normal.z);

    //texture coordinates
    buffer.AddData(2, 0, 0);
    buffer.AddData(2, 0.5, 1);
    buffer.AddData(2, 1, 0);
}


void CMyApp::create2Dplane(gVertexBuffer &_bObj, float size) {
    // create the plane:

    float sizeN = size / 2;

    _bObj.AddAttribute(0, 3); //positions
    _bObj.AddAttribute(1, 3); //normals
    _bObj.AddAttribute(2, 2); // tex coords

    //positions
    _bObj.AddData(0, -sizeN, 0, -sizeN);
    _bObj.AddData(0, sizeN, 0, -sizeN);
    _bObj.AddData(0, -sizeN, 0, sizeN);
    _bObj.AddData(0, sizeN, 0, sizeN);

    // normals
    _bObj.AddData(1, 0, 1, 0);
    _bObj.AddData(1, 0, 1, 0);
    _bObj.AddData(1, 0, 1, 0);
    _bObj.AddData(1, 0, 1, 0);

    // tex coords
    _bObj.AddData(2, 0, 0);
    _bObj.AddData(2, 1, 0);
    _bObj.AddData(2, 0, 1);
    _bObj.AddData(2, 1, 1);

    _bObj.AddIndex(1, 0, 2);
    _bObj.AddIndex(1, 2, 3);

    _bObj.InitBuffers();

}

bool CMyApp::Init() {
    // background color
    glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

    // set cullface and z-buffer ON, cull backfaces
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);



    //Create plane
    create2Dplane(m_vb, planesize);

    //Sphere
    m_vb_sphere.AddAttribute(0, 3);
    m_vb_sphere.AddAttribute(1, 3);
    m_vb_sphere.AddAttribute(2, 2);

    genParametricSurface(sphereResolution, m_vb_sphere);

    m_vb_sphere.InitBuffers();


    //
    // load shaders
    //
    m_program.AttachShader(GL_VERTEX_SHADER, "XmasTree/dirLight.vert");
    m_program.AttachShader(GL_FRAGMENT_SHADER, "XmasTree/dirLight.frag");

    m_program.BindAttribLoc(0, "vs_in_pos");
    m_program.BindAttribLoc(1, "vs_in_normal");
    m_program.BindAttribLoc(2, "vs_in_tex0");

    if (!m_program.LinkProgram()) {
        return false;
    }

    //
    // misc init
    //

    m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

    m_textureID = TextureFromFile("XmasTree/Resources/texture.png");

    m_mesh = ObjParser::parse("XmasTree/Resources/snowfl.obj");
    m_mesh->initBuffers();


    m_textureID_tree = TextureFromFile("XmasTree/Resources/leaves.png");

    m_mesh_tree = ObjParser::parse("XmasTree/Resources/tree.obj");
    m_mesh_tree->initBuffers();


    inittime = SDL_GetTicks() / 1000.0;


    return true;
}

void CMyApp::Clean() {
    glDeleteTextures(1, &m_textureID);

    m_program.Clean();
}

void CMyApp::Update() {
    static Uint32 last_time = SDL_GetTicks();
    float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

    m_camera.Update(delta_time);

    last_time = SDL_GetTicks();
}


void CMyApp::DrawCube(glm::mat4 transf) {
    glm::mat4 matWorld;
    glm::mat4 matWorldIT;
    glm::mat4 mvp;

    m_program.On();

    m_vb_cube.On();


    matWorld = transf * glm::mat4(1.0f);

    matWorldIT = glm::transpose(glm::inverse(matWorld));
    mvp = m_camera.GetViewProj() * matWorld;

    m_program.SetUniform("eye_pos", m_camera.GetEye());
    m_program.SetUniform("index", 1);
    m_program.SetTexture("texImage", 0, m_textureID);
    m_program.SetUniform("world", matWorld);
    m_program.SetUniform("worldIT", matWorldIT);
    m_program.SetUniform("MVP", mvp);


    m_vb_cube.DrawIndexed(GL_TRIANGLES, 0, 36, 0); //IB


    m_vb_cube.Off();
    m_program.Off();
}

void CMyApp::DrawPrism(glm::mat4 transf, int isBird = 0) {
    glm::mat4 matWorld;
    glm::mat4 matWorldIT;
    glm::mat4 mvp;

    m_program.On();

    m_vb_prism.On();


    matWorld = transf * glm::mat4(1.0f);

    matWorldIT = glm::transpose(glm::inverse(matWorld));
    mvp = m_camera.GetViewProj() * matWorld;

    m_program.SetUniform("eye_pos", m_camera.GetEye());
    m_program.SetUniform("index", 1);
    m_program.SetTexture("texImage", 0, m_textureID);
    m_program.SetUniform("world", matWorld);
    m_program.SetUniform("worldIT", matWorldIT);
    m_program.SetUniform("MVP", mvp);
    m_program.SetUniform("isBird", isBird);


    m_vb_prism.DrawIndexed(GL_TRIANGLES, 0, 24, 0); //IB


    m_program.SetUniform("isBird", 0);

    m_vb_prism.Off();
    m_program.Off();
}

void CMyApp::DrawTree(glm::mat4 transf = glm::mat4(1.0f)) {
    glm::mat4 matWorld;
    glm::mat4 matWorldIT;
    glm::mat4 mvp;

    m_program.On();

    matWorld = transf *
               glm::mat4(1.0f);
    matWorldIT = glm::transpose(glm::inverse(matWorld));
    mvp = m_camera.GetViewProj() * matWorld;

    m_program.SetUniform("world", matWorld);
    m_program.SetUniform("worldIT", matWorldIT);
    m_program.SetUniform("MVP", mvp);
    m_program.SetUniform("eye_pos", m_camera.GetEye());
    m_program.SetTexture("texImage", 0, m_textureID_tree);

    m_mesh_tree->draw();
    m_program.Off();
}

void CMyApp::DrawSnow(glm::mat4 transf = glm::mat4(1.0f), glm::mat4 scale = glm::mat4(1.0f)) {

    m_program.On();

    glm::mat4 matWorld;
    glm::mat4 matWorldIT;
    glm::mat4 mvp;

    matWorld =
            transf * scale *
            glm::translate(glm::vec3(0, 3, 0))
            *
            glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0));

    matWorldIT = glm::transpose(glm::inverse(matWorld));
    mvp = m_camera.GetViewProj() * matWorld;

    m_program.SetUniform("world", matWorld);
    m_program.SetUniform("worldIT", matWorldIT);
    m_program.SetUniform("MVP", mvp);
    m_program.SetUniform("eye_pos", m_camera.GetEye());
    m_program.SetTexture("texImage", 0, m_textureID);

    m_program.SetUniform("isSnow", 1);

    m_mesh->draw();

    m_program.SetUniform("isSnow", 0);

    m_program.Off();
}

void CMyApp::Draw2DPlane(glm::mat4 matW = glm::mat4(1.0f)) {

    gShaderProgram &prog = m_program;
    gVertexBuffer &buffer = m_vb;
    gCamera &camera = m_camera;
    GLuint &texutre = m_textureID;

    prog.On();

    glm::mat4 matWorld = matW * glm::mat4(1.0f);
    glm::mat4 matWorldIT = glm::transpose(glm::inverse(matWorld));
    glm::mat4 mvp = camera.GetViewProj() * matWorld;

    prog.SetUniform("world", matWorld);
    prog.SetUniform("worldIT", matWorldIT);
    prog.SetUniform("MVP", mvp);
    prog.SetUniform("eye_pos", camera.GetEye());

    prog.SetTexture("texImage", 0, texutre);

    // draw with VAO
    buffer.On();
    buffer.DrawIndexed(GL_TRIANGLES, 0, 6, 0); //IB
    buffer.Off();

    prog.Off();
}

void CMyApp::DrawSphere(glm::mat4 matW = glm::mat4(1.0f), int clrindex = 0) {
    // draw sphere
    m_program.On();

    glm::mat4 matWorld;
    glm::mat4 matWorldIT;
    glm::mat4 mvp;

    matWorld = matW * glm::rotate<float>(2 * M_PI * SDL_GetTicks() / 5000.0f, glm::vec3(0, 1, 0));
    matWorldIT = glm::transpose(glm::inverse(matWorld));
    mvp = m_camera.GetViewProj() * matWorld;

    m_program.SetUniform("world", matWorld);
    m_program.SetUniform("worldIT", matWorldIT);
    m_program.SetUniform("MVP", mvp);
    m_program.SetUniform("eye_pos", m_camera.GetEye());

    m_program.SetTexture("texImage", 0, m_textureID);
    m_program.SetUniform("isSphere", 1);
    m_program.SetUniform("sphereKd", sphereColors.at(clrindex));




    // draw with VAO
    m_vb_sphere.On();

    m_vb_sphere.DrawIndexed(GL_TRIANGLES, 0, sphereResolution * sphereResolution * 6, 0);

    m_vb_sphere.Off();
    m_program.SetUniform("isSphere", 0);


    m_program.Off();
}

void CMyApp::Render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //Transformation matrices


    //Drawing the plane
    Draw2DPlane();


    // draw snow
    glm::mat4 topsnowalign = glm::translate<float>(glm::vec3(0, 13, 0)) * glm::scale<float>(glm::vec3(0.5, 0.5, 0.5));

    DrawSnow(topsnowalign);
    //std::cout << time << std::endl;
    //Draw n snow:
    for (size_t i = 0; i < snowcnt; i++) {
        float speed = snowYarr.at(i);
        float startY = 20.0; // distance
        float endY = 0;

        int timetofall = startY / speed;

        float flaketime = (SDL_GetTicks() % (timetofall * 1000)) / 1000.0; //0- 20 arasi float saniye
        //std::cout << flaketime << std::endl;

        float Y = startY - flaketime * speed;//10 * sin(flaketime);//between startY and endY

        glm::mat4 translateMat = glm::translate(glm::vec3(snowXarr.at(i), Y, snowZarr.at(i)));
        glm::mat4 scaleMat = glm::scale(glm::vec3(0.125, 0.125, 0.125));
        DrawSnow(translateMat, scaleMat);

    }

    //draw tree
    glm::mat4 treealign =
            glm::translate<float>(glm::vec3(-0.0550, 5.5, -0.125)) * glm::scale<float>(glm::vec3(3, 3, 3));
    DrawTree(treealign);

    //draw sphere //glm::translate<float>(glm::vec3(cos(2*M_PI /9) ,0, sin(2 * M_PI / 9))) *
    for (size_t level = 1; level < 7; level++) {
        for (size_t i = 0; i < 9; i++) {
            float r = 3.5 - 0.5 * level;
            if (level >= 4) {

                r += 0.25;

            }
            float fi = (2 * M_PI) / 9.0;

            glm::mat4 spherealign =
                    glm::translate<float>(glm::vec3(r * sin(fi * i), 2 + 1.75 * level, r * cos(fi * i)))
                    * glm::scale<float>(glm::vec3(0.125, 0.125, 0.125));

            DrawSphere(spherealign, i + level);
        }

    }


}

void CMyApp::KeyboardDown(SDL_KeyboardEvent &key) {
    m_camera.KeyboardDown(key);
    switch (key.keysym.sym) {
        /*case SDLK_1:
            selected = 0;
            break;
        case SDLK_2:
            selected = 1;
            break;
        case SDLK_3:
            selected = 2;
            break;
    */
        case SDLK_UP:
            scareCrowPosition.z -= 1;
            break;
        case SDLK_DOWN:
            scareCrowPosition.z += 1;
            break;
        case SDLK_LEFT:
            scareCrowPosition.x -= 1;
            break;
        case SDLK_RIGHT:
            scareCrowPosition.x += 1;
            break;
        case SDLK_SPACE:
            stomping = true;
            t0 = SDL_GetTicks();
            break;
        default:
            break;
    }
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent &key) {
    m_camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent &mouse) {
    m_camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent &mouse) {
}

void CMyApp::MouseUp(SDL_MouseButtonEvent &mouse) {
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent &wheel) {
}

void CMyApp::Resize(int _w, int _h) {
    glViewport(0, 0, _w, _h);

    m_camera.Resize(_w, _h);
}
